<?php
require_once __DIR__ ."/top-splash.php";
?>

    <div class="modal-form-cover">
        <div class="login-box login">
            <div class="login-left">
                <a href="index"> <img class="login-dog" src="img-website/dog.png" alt="dog"></a>
            </div>
            <div class="signup-login-right">
            <h1 class="form-title-login">Log in</h1>
            <form id="frmLogin" class="signup-login-form" method="POST">
                <input name="txtLoginNickname" placeholder="nickname" type="text"
                       data-validate="yes" data-type="string" data-min="8" data-max="30">
                <input name="txtLoginPassword" placeholder="password" type="password"
                       data-validate="yes" data-type="string" data-min="4" data-max="50">
                <button class="basic">LOGIN</button>
                <a class="link" href="signup"> Sign up</a>
            </form>
            </div>
        </div>
    </div>
<?php
$sLinkToScript = '<script src="js/login.js"></script>';
require_once __DIR__ ."/bottom.php";