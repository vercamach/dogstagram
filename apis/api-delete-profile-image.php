<?php
ini_set('display_errors',0 );
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: ../login');
  }
$sUserId = $_SESSION['sUserId'];

if(empty($_GET['sProfileUserId'])){sendResponse(0, __LINE__, "Couldn't get the users id");};

$sUserProfileId = $_GET['sProfileUserId'];

if($sUserId !== $sUserProfileId){sendResponse(0, __LINE__, "User id doesn't match"); }

require_once __DIR__.'/../connect.php';

try{
    $stmt = $db->prepare('DELETE FROM profile_images WHERE user_fk = :sUserProfileId');
    $stmt->bindValue(':sUserProfileId', $sUserProfileId);
    $stmt->execute();
    $sRowsAffected = $stmt->rowCount();
}catch( PDOException $e){
    echo $e;
    exit();
}

if($sRowsAffected !== 1){
    sendResponse(0,__LINE__,"Couldn't delete the image");
}


sendResponse(1,__LINE__,"Your image was deleted");

function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}
