<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <title>Dogstagram <?= $sInjectUserProfileName ?? '' ?></title>
</head>
<body class="body-main">
    <div class="nav-wrapper">
        <a class="nav small home" href="dogstagram">
            <img class="dog-house-icon" src="img-website/dog-house-icon.png" alt="">#WHOSAGOODBOY</a>
        <div id="mobile-menu">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#9776;</a>
                <div class="nav-links">
                    <a  href="admin-images-view.php" onclick="closeNav()">Uploaded images</a>
                    <a  onclick="closeNav()" href="admin-users-view.php">Users</a>
                    <a href="logout.php" onclick="closeNav()">Log out</a>
                </div>
            </div>
            <span id="openNav" onclick="openNav() ">&#9776;</span>
        </div>

        <div class="nav">
            <a class="nav small <?= $sInjectAdminImagesViewActiveClass ?? '' ?>"   href="admin-images-view.php">Uploaded images</a>
            <a class="nav small <?= $sInjectAdminUsersViewActiveClass ?? '' ?>"    href="admin-users-view.php">Users</a>
            <a class="nav small" href="logout.php">Log out</a>
        </div>
    </div>
