<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('SELECT image_id, max(total) from (SELECT image_fk 
    as image_id, sum(points_amount) 
    as total FROM points 
    group by image_fk) as max_total_points ');
    $stmt->execute();
    $aRows = $stmt->fetch();
    $sImageId = $aRows->image_id;
    echo json_encode($sImageId);
} catch (PDOException $e) {
    echo $e;
    exit();
}
try {
    $stmt = $db->prepare('SELECT * from images where id = :sImageId');
    $stmt->bindValue(':sImageId', $sImageId);
    $stmt->execute();
    $aImageRow = $stmt->fetch();
    echo json_encode($aImageRow);

} catch (PDOException $e) {
    echo $e;
}