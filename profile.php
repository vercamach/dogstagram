<?php
ini_set('display_errors', 0);
$sInjectProfileActiveClass="active";
require_once __DIR__ . '/top.php';

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

require_once __DIR__ . '/connect.php';
try{
$stmtShowProfile = $db->prepare('SELECT * FROM users_profile_details WHERE id = :sUserId');
$stmtShowProfile->bindValue(':sUserId', $sUserId);
$stmtShowProfile->execute();
$iUserRow = $stmtShowProfile->fetch();
} catch (PDOException $e) {
    echo $e;
}
?>
    <div class="details-section">
        <?php
        if ($iUserRow->url == null) {
            echo '

        <form class="upload-form-profile" action="apis/api-upload-image-profile.php" method="POST"
              enctype="multipart/form-data">
            <label id="label" for="profileImageToUpload" class="profile-upload-img">
                <img class="upload-icon"  src="icons/upload-img-profile.png" alt="upload-icon">
            </label>
            <input type="file" name="profileImageToUpload" id="profileImageToUpload">
            <img id="thumbnail-profile" src="#" alt="your image" class="thumbnail-profile hidden"/>
            <input type="submit" value="Upload Image" name="submit" id="submit" class="hi submit-profile-button hidden">
        </form>
        
    
    ';
        } else {
            echo '
        <div class="profile-img">
            <div data-user-id="' . $sUserId . '" class="round round-profile" onclick="deleteProfileImg(this)">   <img class="delete-icon" src="icons/delete.png" alt=""></div>
            <img class="profile-img" src="img/' . $iUserRow->url . '" alt="">
        </div>';
        }
        ?>
        <div class="profile-info" id="profileInformation">
            <p class="profile-text">name: <?= $iUserRow->name ?> </p>
            <p class="profile-text">nickname: <?= $iUserRow->nickname ?></p>
            <p class="profile-text">e-mail: <?= $iUserRow->email ?></p>
            <div class="profile-buttons">
                <button class="profile" id="btnChangeInformation" onclick="showUpdateInfoForm()">Change information
                </button>
                <button class="profile" id="btnChangePassword" onclick="showUpdatePasswordForm()">Change password
                </button>
            </div>
        </div>
        <div class="content-add-info hidden" id="frmContainerChangeInfo">
            <form method="POST" id="frmChangeProfileInformation">
                <input class="add-info" name="sUserId" type="hidden" placeholder="id" value="<?= $sUserId ?>">
                <input class="add-info" name="sUserName" type="text" placeholder=<?= $iUserRow->name ?>>
                <input class="add-info" name="sUserNickName" type="text" placeholder=<?= $iUserRow->nickname ?>>
                <input class="add-info" name="sUserEmail" type="text" placeholder=<?= $iUserRow->email ?>>
                <button class="basic">Save</button>
            </form>
        </div>
        <div class="content-add-info hidden" id="frmContainerChangePassword">
            <form method="POST" id="frmChangePassword">
                <input class="add-info" name="sOldPassword" type="password" placeholder="Old password">
                <input class="add-info" name="sNewPassword" type="password" placeholder="New password">
                <input class="add-info" name="sConfirmNewPassword" type="password" placeholder="Repeat new password">
                <button class="basic">Save</button>
            </form>
        </div>
    </div>
<?php

$sLinkToScript = '<script src="js/profile.js"></script>';
require_once __DIR__ . '/bottom.php';
