$("#frmSignup").submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-signup",
        data: $("#frmSignup").serialize(),
        dataType: "JSON"
    })
        .done(function (jData) {
            if (jData.status === 1) {
                swal({
                    title: "Go activate your e-mail.",
                    icon: "success"
                }).then(function () {
                    window.location = "login";
                });
            } else {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                });
            }
        })
        .fail(function () {
            console.log("error");
        });
    return false;
});