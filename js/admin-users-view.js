function blockUser(e) {
    var sBlockedUserId = e.getAttribute('data-user-id');

    $.ajax({
        method: "GET",
        url: "apis/api-admin-block-unblock-user.php",
        data: {
            "sBlockedUserId": sBlockedUserId
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status === 1) {
            swal({
                title: jData.message,
                icon: "success"
            }).then(function () {
                window.location.reload();
            })
        } else {
            swal({
                title: jData.message,
                icon: "warning"
            })
        }
    }).fail(function () {
        console.log('error')
    });


}


