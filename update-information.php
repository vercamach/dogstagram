<?php
ini_set('display_errors', 0);
$sInjectUploadImgActiveClass="active";
require_once __DIR__ . '/top.php';
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sImageId = $_GET['iImageId'];
if (empty($_GET['iImageId'])) {
    sendResponse(0, __LINE__, 'Image ID is missing');
}

require_once __DIR__ . '/connect.php';
try {
    $stmt = $db->prepare('SELECT * FROM images WHERE id = :sImageId');
    $stmt->bindValue(':sImageId', $sImageId);
    $stmt->execute();
    $aRow = $stmt->fetch();
} catch (PDOException $e) {
    echo $e;
}
?>
    <div class="split-container add-info-container">
        <?php
        echo '
    <div class="image small">
      <img class="photo-small" src="img/' . $aRow->url . '" alt="img">
    </div>
    ';
        ?>
        <div class="content-add-info">
            <h2 class="">Update details</h2>
            <form id="frmAddDescription" method="POST">
                <input class="add-info" name="sImageId" type="hidden" placeholder="id" value="<?= $sImageId ?>">
                <input class="add-info" name="sDogName" type="text" placeholder="name">
                <input class="add-info text-area" name="sDogDescription" type="text" placeholder="description">
                <button class="basic">Save</button>
            </form>
        </div>
    </div>

<?php
$sLinkToScript = '<script src="js/add-update-description.js"></script>';
require_once __DIR__ . '/bottom.php';

function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status":' . $iStatus . ', "code":' . $iLineNumber . ',"message":"' . $sMessage . '"}';
    exit;
}
