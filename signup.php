<?php
require_once __DIR__ . "/top-splash.php";
?>

    <div class="modal-form-cover">
        <div class="login-box signup-box">
            <div class="login-left">
                <a href="index"> <img class="login-dog" src="img-website/dog.png" alt="dog"></a>
            </div>
            <div class="signup-login-right signup">
                <h1 class="form-title-signup">Sign up</h1>
                <form action="apis/api-signup" id="frmSignup" method="POST">
                    <input name="txtSignupName" type="text" placeholder="name"
                           data-validate="yes" data-type="string" data-min="8" data-max="50">
                    <input name="txtSignupNickname" type="text" placeholder="nickname"
                           data-validate="yes" data-type="string" data-min="8" data-max="30">
                    <input name="txtSignupEmail" type="text" placeholder="e-mail"
                           data-validate="yes" data-type="email" data-min="8" data-max="100">
                    <input name="txtSignupPassword" type="password" placeholder="password"
                           data-validate="yes" data-type="string" data-min="4" data-max="100">
                    <input name="txtSignupConfirmPassword" type="password" placeholder="confirm password"
                           data-validate="yes" data-type="string" data-min="4" data-max="100">
                    <button class="basic">SIGN UP</button>
                    <a class="link" href="login">Log in</a>
                </form>
            </div>
        </div>
    </div>
<?php
$sLinkToScript = '<script src="js/signup.js"></script>';
require_once __DIR__ . "/bottom.php";
