<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: ../login');
}
$sUserId = $_SESSION['sUserId'];

require_once __DIR__ . '/../connect.php';
try {
    $stmtGetOldPassword = $db->prepare('SELECT password FROM users where id = :sUserId');
    $stmtGetOldPassword->bindValue(':sUserId', $sUserId);
    $stmtGetOldPassword->execute();
    $aOldHashedPassword = $stmtGetOldPassword->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}


$sOldPassword = $_POST['sOldPassword'];
if (!password_verify($sOldPassword, $aOldHashedPassword->password)) {
    sendResponse(0, __LINE__, 'Old password is wrong');
}


$sNewPassword = $_POST['sNewPassword'];
if (empty($sNewPassword)) {
    sendResponse(0, __LINE__, 'New password missing');
}
if (strlen($sNewPassword) < 4) {
    sendResponse(0, __LINE__, 'New password too short');
}
if (strlen($sNewPassword) > 50) {
    sendResponse(0, __LINE__, 'New password too long');
}

$sConfirmNewPassword = $_POST['sConfirmNewPassword'];
if (empty($sConfirmNewPassword)) {
    sendResponse(0, __LINE__, 'Confirm password missing');
}
if ($sNewPassword != $sConfirmNewPassword) {
    sendResponse(0, __LINE__, "Passwords don't match");
}

try {
    $stmtChangePassword = $db->prepare('UPDATE users SET password = :sNewPassword WHERE id = :sUserId');
    $stmtChangePassword->bindValue(':sNewPassword', password_hash($sNewPassword, PASSWORD_DEFAULT));
    $stmtChangePassword->bindValue('sUserId', $sUserId);
    $stmtChangePassword->execute();
    $iRowAffected = $stmtChangePassword->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($iRowAffected === 1) {
    sendResponse(1, __LINE__, 'Your password had been changed');
};


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message":"' . $sMessage . '"}';
    exit;

}