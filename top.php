<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <title>Dogstagram</title>
</head>
<body class="body-main">
    <div class="nav-wrapper">
        <a class="nav small home" href="dogstagram">
            <img class="dog-house-icon" src="img-website/dog-house-icon.png" alt="">#WHOSAGOODBOY</a>
        <div id="mobile-menu">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#9776;</a>
                <div class="nav-links">
                    <a  href="dogstagram" onclick="closeNav()">Home</a>
                    <a  href="uploaded-images" onclick="closeNav()">Upload a dog</a>
                    <a onclick="closeNav()" href="image-of-the-day">Boy of the day</a>
                    <a onclick="closeNav()" href="profile">Profile</a>
                    <a onclick="closeNav()" href="admin-images-view.php">Admin</a>
                    <a href="logout.php" onclick="closeNav()">Log out</a>
                </div>
            </div>
            <span id="openNav" onclick="openNav() ">&#9776;</span>
        </div>
        <div class="nav">
            <a class="nav small <?= $sInjectHomeActiveClass ?? '' ?>"  href="dogstagram">Home</a>
            <a class="nav small <?= $sInjectUploadImgActiveClass ?? '' ?>"  href="uploaded-images">Upload a dog</a>
            <a class="nav small <?= $sInjectBoyOfTheDayActiveClass ?? '' ?>"   href="image-of-the-day">Boy of the day</a>
            <a class="nav small <?= $sInjectProfileActiveClass ?? '' ?>"   href="profile">Profile</a>
            <a class="nav small"  href="admin-images-view.php">Admin</a>
            <a class="nav small" href="logout.php">Log out</a>
        </div>
    </div>
