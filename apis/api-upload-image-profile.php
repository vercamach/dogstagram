<?php
session_start();
ini_set('display_errors', 0);
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];


$target_dir = "../img/";
$target_file = $target_dir . basename(uniqid() . $_FILES["profileImageToUpload"]["name"]);


$sImageUrl = basename($target_file);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["profileImageToUpload"]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check file size
if ($_FILES["profileImageToUpload"]["size"] > 5000000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["profileImageToUpload"]["tmp_name"], $target_file)) {

        require_once __DIR__ . '/../connect.php';
        try {
            $stmt = $db->prepare("INSERT INTO profile_images (user_fk, url) VALUES ( :sUserFk,   :sUrl);");
            $stmt->bindValue(':sUserFk', $sUserId);
            $stmt->bindValue(':sUrl', $sImageUrl);
            $stmt->execute();
            $aUploadedImg = $stmt->rowCount();
        } catch (PDOException $e) {
            echo $e;
        }
        header('Location: ../profile');
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}



