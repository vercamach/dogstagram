<?php
ini_set('display_errors', 0);
$sInjectBoyOfTheDayActiveClass="active";
require_once __DIR__ . '/top.php';

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];


require_once __DIR__ . '/connect.php';

try {
    $stmtSelectTopScore = $db->prepare('SELECT total, image_fk as image_id FROM
    (SELECT points.image_fk, SUM(points.points_amount) AS total 
    FROM points GROUP BY image_fk) AS total GROUP BY total DESC limit 1');
    $stmtSelectTopScore->execute();
    $iRowsAffected = $stmtSelectTopScore->rowCount();
    $aTopTotalRow = $stmtSelectTopScore->fetch();
} catch (PDOException $e) {
    echo $e;
}


if ($iRowsAffected !== 1) {
    echo "
            <h1 class='empty-page-message'>It looks like we are missing a champion!</h1>
            ";
    require_once __DIR__ . '/bottom.php';
    $sLinkToScript = '<script src="js/image-of-the-day.js"></script>';
    exit();
}

$aPointsAmount = $aTopTotalRow->total;
$sImageId = $aTopTotalRow->image_id;

try {
    $stmtShowImgOfTheDay = $db->prepare
    ('SELECT images.url, dogs_descriptions.image_fk, 
    dogs_descriptions.description, 
    UCASE(dogs_descriptions.name) as name_uppercase 
    FROM dogs_descriptions 
    JOIN images ON dogs_descriptions.image_fk = images.id 
    WHERE images.id = :sImageId');
    $stmtShowImgOfTheDay->bindValue(':sImageId', $sImageId);
    $stmtShowImgOfTheDay->execute();
    $aBoyOfTheDay = $stmtShowImgOfTheDay->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}
?>
    <iframe src="silence.mp3" allow="autoplay" style="display:none" id="iframeAudio">
    </iframe>
    <audio id="player" autoplay loop>
        <source src="audio/celebration-short.mp3" type="audio/mp3">
    </audio>
<?php

echo '
<div class="baloon-cover-wrapper">
    <img class="baloon-cover" src="img-website/baloons-cover.png" alt="">
</div>
    <div class="images-container">
        <div class="image winner-image">
             <img class="img-of-the-day" id="best-img" src="img/' . $aBoyOfTheDay->url . '" alt="img">
         </div>
    <div class="content">
    <h2>' . $aBoyOfTheDay->name_uppercase . ' </h2>
    <p>' . $aBoyOfTheDay->description . '</p>

        </div>
    </div>
';


require_once __DIR__ . '/bottom.php';
$sLinkToScript = '<script src="js/image-of-the-day.js"></script>';
