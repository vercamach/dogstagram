<?php
ini_set('display_errors', 0);
$sInjectUploadImgActiveClass="active";
require_once __DIR__ . '/top.php';

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];
echo $sUserId;

require_once __DIR__ . '/connect.php';
try {
    $stmt = $db->prepare('SELECT * FROM images WHERE user_fk = :iUserId  LIMIT 30');
    $stmt->bindValue(':iUserId', $sUserId);
    $stmt->execute(); // Check if this works
    $aRows = $stmt->fetchAll();
} catch (PDOException $e) {
    echo $e;
}

if (sizeof($aRows) == 0) {
    echo "
            <h1 class='empty-page-message'>It looks like you haven't uploaded any images yet</h1>
            ";
}
?>
    <div class="upload-img">
        <form class="upload-form" action="apis/api-upload-image-dog.php" method="post" enctype="multipart/form-data">
            <label for="fileToUpload" class="custom-file-upload" id="label-upload">
                <img style="width: 90px" src="icons/upload.svg" alt="upload-icon">
            </label>
            <input type="file" name="fileToUpload" id="fileToUpload">
            <img id="thumbnail" src="#" alt="your image" class="thumbnail hidden"/>
            <input type="submit" value="Upload Image" name="submit" id="submit" class="hi submit-upload-button hidden">
        </form>
    </div>


    <div class="grid-container-wrapper">
        <div class="images-container-grid">
            <?php
            foreach ($aRows as $jRow) {
                echo '
            <div class="image image-grid-wrapper">
                 <div data-image-id="' . $jRow->id . '" class="round" onclick="deleteImg(this)">  
                    <img class="delete-icon" src="icons/delete.png" alt="">
                 </div>
                 <img class="photo" src="img/' . $jRow->url . '" alt="img">
                 <a class="login-signup add-information" href="update-information?iImageId=' . $jRow->id . '">
                    <img class="icon" src="icons/add-icon.png" alt="">
                 </a>
    
            </div>
    ';
            }
            ?>
        </div>
    </div>
<?php
$sLinkToScript = '<script src="js/uploaded-images.js"></script>';
require_once __DIR__ . '/bottom.php';
