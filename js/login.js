$("#frmLogin").submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-login",
        data: $("#frmLogin").serialize(),
        dataType: "JSON"
    })
        .done(function (jData) {
            if (jData.status == 0) {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                });
                return;
            }
            location.href = "dogstagram";
        })
        .fail(function () {
            console.log("error");
        });
    return false;
});
