<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('SELECT images.id as image_id, points.points_amount, 
    COUNT(points.points_amount) as total 
    FROM images
    JOIN points ON images.id = points.image_fk 
    GROUP BY images.id, points.points_amount');
    $stmt->execute();
    $aRows = $stmt->fetchAll();
} catch (PDOException $e) {
    echo $e;
    exit();
}


echo json_encode($aRows);

