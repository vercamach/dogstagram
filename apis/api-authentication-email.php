<?php
ini_set('display_errors', 0);

if (empty($_GET['activationEmail'])) {
    sendResponse(0, __LINE__, 'No activation Email');
}
$sActivationEmail = $_GET['activationEmail'];

if (empty($_GET['activationKey'])) {
    sendResponse(0, __LINE__, 'No activation key');
}
$sActivationKey = $_GET['activationKey'];


require_once __DIR__ . '/../connect.php';
try {
    $stmtSelectUserToActivate = $db->prepare('SELECT * FROM users where email = :sActivateEmail AND  activation_key = :sActivationKey');
    $stmtSelectUserToActivate->bindValue(':sActivateEmail', $sActivationEmail);
    $stmtSelectUserToActivate->bindValue(':sActivationKey', $sActivationKey);
    $stmtSelectUserToActivate->execute();
    $iRowAmount = $stmtSelectUserToActivate->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if ($iRowAmount !== 1) {
    sendResponse(0, __LINE__, "Couldn't activate user.");
}

try {
    $stmtActivateUser = $db->prepare('UPDATE users SET active = 1 WHERE email = :sActivateEmail AND  activation_key = :sActivationKey');
    $stmtActivateUser->bindValue(':sActivateEmail', $sActivationEmail);
    $stmtActivateUser->bindValue(':sActivationKey', $sActivationKey);
    $stmtActivateUser->execute();
    $iRowAmountActivated = $stmtSelectUserToActivate->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($iRowAmountActivated === 1) {
    header('Location: ../login');
}


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status":' . $iStatus . ', "code":' . $iLineNumber . ',"message":"' . $sMessage . '"}';
    exit;
}



