<?php
ini_set('display_errors', 0);
require_once __DIR__ . '/top.php';

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}

$sUserId = $_SESSION['sUserId'];

$sImgId = $_GET['sImgId'];
if (empty($sImgId)) {
    header('Location: dogstagram.php');
}

require_once __DIR__ . '/connect.php';

try {
    $stmtGetImgDetails = $db->prepare('SELECT * FROM dogs_detail_views where dog_id = :sImageId');
    $stmtGetImgDetails->bindValue(':sImageId', $sImgId);
    $stmtGetImgDetails->execute();
    $aRowImgDetails = $stmtGetImgDetails->fetch();

} catch (PDOException $e) {
    echo $e;
    exit();
}

try {
    $stmtGetUserProfile = $db->prepare('SELECT profile_images.url FROM profile_images where user_fk = :sUserId');
    $stmtGetUserProfile->bindValue(':sUserId', $sUserId);
    $stmtGetUserProfile->execute();
    $iProfileImg = $stmtGetUserProfile->rowCount();
    $sProfileImgUrl = $stmtGetUserProfile->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}
try {
    $iGetTotalPoints = $db->prepare('SELECT SUM(points_amount) AS total FROM points WHERE points.image_fk = :sImageId');
    $iGetTotalPoints->bindValue(':sImageId', $sImgId);
    $iGetTotalPoints->execute();
    $iTotalPoints = $iGetTotalPoints->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}
?>
    <div class="img-detail">
        <div class="images-container">
            <div class="image">
                <div class="image-user">
                    <div class="user-min">
                        <?php
                        if ($iProfileImg == 0) {
                            echo ' <div><img  class="profile-img-min" src="img-website/dog-icon.png" alt=""></div>';

                        } else {
                            echo ' <div><img  class="profile-img-min" src="img/' . $sProfileImgUrl->url . '" alt=""></div>';
                        }
                        ?>

                        <p class="user-nickname"><?= $aRowImgDetails->user_nickname ?></p>
                    </div>
                </div>
                <img class="dog-img-detail" src="img/<?= $aRowImgDetails->dog_image ?>" alt="">
                <div class="img-profile-text">
                    <h4>name: <?= $aRowImgDetails->dog_name ?></h4>
                    <?php
                    if ($iTotalPoints->total === NULL) {
                        echo ' <h4>total points: 0</h4>';

                    } else {
                        echo '<h4>total points:' . $iTotalPoints->total . '</h4>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>


<?php
require_once __DIR__ . '/bottom.php';
