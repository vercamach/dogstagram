<?php
ini_set('display_errors', 0);
$sInjectUploadImgActiveClass="active";
require_once __DIR__ . '/top.php';
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}

$sUserId = $_SESSION['sUserId'];

if (!isset($_SESSION['sImageUrl'])) {
    header('Location: login');
}
$sImageUrl = $_SESSION['sImageUrl'];

require_once __DIR__ . '/connect.php';
try {
    $stmtSelectUploadedImgId = $db->prepare('SELECT * FROM images WHERE url = :sUrl');
    $stmtSelectUploadedImgId->bindValue(':sUrl', $sImageUrl);
    $stmtSelectUploadedImgId->execute();
    $aRow = $stmtSelectUploadedImgId->fetch();
    $sImageId = $aRow->id;
} catch (PDOException $e) {
    echo $e;
}

echo '
    <div class="split-container add-info-container">
    <div class="image small">
      <img class="photo-small" src="img/' . $aRow->url . '" alt="img">
    </div>
    ';

?>
    <div class="content-add-info">
        <h2 class="add-details">Add details</h2>
        <form method="POST" id="frmAddDescription">
            <input class="add-info" name="sImageId" type="hidden" placeholder="id" value="<?= $sImageId ?>">
            <input class="add-info" name="sDogName" type="text" placeholder="name">
            <input class="add-info text-area" name="sDogDescription" type="text" placeholder="hashtags">
            <button class="basic">Save</button>
        </form>
    </div>
    </div>

<?php
$sLinkToScript = '<script src="js/add-update-description.js"></script>';
require_once __DIR__ . '/bottom.php';
