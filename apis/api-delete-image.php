<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: ../login');
}
$sUserId = $_SESSION['sUserId'];
if (empty($_GET['sImageId'])) {
    sendResponse(0, __LINE__, "Couldn't get the image id");
};
$sImageId = $_GET['sImageId'];

require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('DELETE FROM images WHERE id = :sImageId AND user_fk = :sUserId');
    $stmt->bindValue(':sImageId', $sImageId);
    $stmt->bindValue(':sUserId', $sUserId);
    $stmt->execute();
    $sRowsAffected = $stmt->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($sRowsAffected !== 1) {
    sendResponse(0, __LINE__, "Couldn't delete the image");
}


sendResponse(1, __LINE__, "Your image was deleted");

function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}
