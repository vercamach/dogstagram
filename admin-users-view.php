<?php
ini_set('display_errors', 0);
$sInjectAdminUsersViewActiveClass="active";
require_once __DIR__ . '/top-admin.php';

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once __DIR__ . '/connect.php';
$stmtGetAllUsers = $db->prepare('SELECT * FROM users');
$stmtGetAllUsers->execute();
$aRowsUsers = $stmtGetAllUsers->fetchAll();
?>
    <div class="users-wrapper">
        <h1 class="admin-title">USERS</h1>

        <table>
            <tr class="head table-row">
                <th>Id</th>
                <th>Name</th>
                <th>Nickname</th>
                <th>E-mail</th>
                <th class="icon-head-cell">&nbsp</th>
            </tr>
            <?php
            foreach ($aRowsUsers as $aRowUser) {
                $sStatus = $aRowUser->active == 0 ? 'blocked' : 'active';

                echo '
                     
                 <tr class="table-row">
                    <td>' . $aRowUser->id . '</td>
                    <td>' . $aRowUser->name . '</td> 
                    <td>' . $aRowUser->nickname . '</td>
                    <td>' . $aRowUser->email . '</td>
                    <td class="icon-cell">  
                    <div data-user-id="' . $aRowUser->id . '" class="round round-admin" onclick="blockUser(this)">  
                    <img class="delete-icon" src="icons/delete.png" alt="">
                 </div>
                
                 <div class="blocked-unblocked ' . $sStatus . '"></div>
                    </td>
                 </tr>    
                 ';
            };
            ?>

        </table>
    </div>


<?php
$sLinkToScript = '<script src="js/admin-users-view.js"></script>';
require_once __DIR__ . '/bottom.php';
