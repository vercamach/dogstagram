<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$target_dir = "../img/";
$target_file = $target_dir . basename(uniqid() . $_FILES["fileToUpload"]["name"]);

$sTitle = uniqid();

$sImageUrl = basename($target_file);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
        header('Location: ../uploaded-images');
    }
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        require_once __DIR__ . '/../connect.php';
        try {
            $stmt = $db->prepare("INSERT INTO images (id, url, user_fk, uploaded_date) VALUES (NULL,  :sUrl, :sUserId, CURRENT_TIMESTAMP());");
            $stmt->bindValue(':sUrl', $sImageUrl);
            $stmt->bindValue(':sUserId', $sUserId);
            $stmt->execute(); // Check if this works
            $aUploadedImg = $stmt->rowCount();
        } catch (PDOException $e) {
            echo $e;
        }
        $_SESSION['sImageUrl'] = $sImageUrl;
        header('Location: ../add-image-description');
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}



