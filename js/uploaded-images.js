function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e.target);
            $('#thumbnail').removeClass('hidden').attr('src', e.target.result);
            $('#submit').removeClass('hidden');

            if ($(window).width() <= 500) {
                $('#label-upload').addClass('hidden');
            }
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$("#fileToUpload").change(function () {
    readURL(this);
});


function deleteImg(e) {
    var sImageId = e.getAttribute('data-image-id');
    console.log(sImageId);

    $.ajax({
        method: "GET",
        url: "apis/api-delete-image.php",
        data: {
            "sImageId": sImageId
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        swal({
            title: "Your image was deleted!",
            icon: "success"
        }).then(function () {
            window.location.reload();
        })
    }).fail(function () {
        console.log('error')
    });

}