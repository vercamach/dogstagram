function deleteImg(e) {
    var sImageId = e.getAttribute('data-image-id');
    console.log(sImageId);

    $.ajax({
        method: "GET",
        url: "apis/api-admin-delete-img.php",
        data: {
            "sImageId": sImageId
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status === 1) {
            swal({
                title: jData.message,
                icon: "success"
            }).then(function () {
                window.location.reload();
            })
        } else {
            swal({
                title: jData.message,
                icon: "warning"
            })
        }
    }).fail(
        function () {
            console.log('error');
        });
}