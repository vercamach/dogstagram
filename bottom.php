<script>

    function openNav() {
        document.getElementById("mySidenav").style.height = "100%";
        document.getElementById("openNav").classList.add("hidden");
    }

    function closeNav() {
        document.getElementById("mySidenav").style.height = "0";
        document.getElementById("openNav").classList.remove("hidden");
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<? echo $sLinkToScript ?? ''; ?>
<script src="js/validate.js"></script>
</body>
</html>