function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e.target);
            $('#label').addClass('hidden');
            $('#thumbnail-profile').removeClass('hidden').attr('src', e.target.result);
            $('#submit').removeClass('hidden');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#profileImageToUpload").change(function () {
    readURL(this);
});


function deleteProfileImg(e) {
    var sProfileUserId = e.getAttribute('data-user-id');
    console.log(sProfileUserId);
    $.ajax({
        method: "GET",
        url: "apis/api-delete-profile-image.php",
        data: {
            "sProfileUserId": sProfileUserId
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status === 1) {
            swal({
                title: jData.message,
                icon: "success"
            }).then(function () {
                window.location.reload();
            })
        } else {
            console.log(jData)
        }
    }).fail(function () {
        console.log('error')
    });

}


function showUpdateInfoForm() {
    document.getElementById('btnChangeInformation').classList.add('hidden');
    document.getElementById('profileInformation').classList.add('hidden');
    document.getElementById('frmContainerChangeInfo').classList.remove('hidden');
};

function showUpdatePasswordForm() {
    document.getElementById('btnChangePassword').classList.add('hidden');
    document.getElementById('profileInformation').classList.add('hidden');
    document.getElementById('frmContainerChangePassword').classList.remove('hidden');
};


$('#frmChangeProfileInformation').submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-update-profile-information.php",
        data: $("#frmChangeProfileInformation").serialize(),
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status === 1) {
            swal({
                title: jData.message,
                icon: "success"
            }).then(function () {
                window.location.reload();
            })
        } else {
            swal({
                title: jData.message,
                icon: "warning"
            })

        }
    }).fail(function () {
        console.log('error')
    });


    return false;
});


$('#frmChangePassword').submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-change-password.php",
        data: $("#frmChangePassword").serialize(),
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status === 1) {
            swal({
                title: jData.message,
                icon: "success"
            }).then(function () {
                window.location.reload();
            })
        } else {
            swal({
                title: jData.message,
                icon: "warning"
            })
        }
    }).fail(
        function () {
            console.log('error');
        });
    return false;
});