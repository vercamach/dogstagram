<?php
ini_set('display_errors', 0);
$sInjectAdminImagesViewActiveClass="active";
require_once __DIR__ . '/top-admin.php';
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once __DIR__ . '/connect.php';
try {
    $stmtGetAllPictures = $db->prepare('SELECT * FROM images');
    $stmtGetAllPictures->execute();
    $aRowImages = $stmtGetAllPictures->fetchAll();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if (sizeof($aRowImages) == 0) {
    echo "
    <h1 class='empty-page-message'>There are no images in the system.</h1>
    ";
}
?>
    <div class="grid-container-wrapper">
        <div class="images-container-grid">
            <?php
            foreach ($aRowImages as $jRow) {
                echo '
            <div class="image image-grid-wrapper">
                 <div data-image-id="' . $jRow->id . '" class="round" onclick="deleteImg(this)">  
                    <img class="delete-icon" src="icons/delete.png" alt="">
                 </div>
                 <img class="photo" src="img/' . $jRow->url . '" alt="img">
            </div>
    ';
            }
            ?>

        </div>
    </div>

<?php

$sLinkToScript = '<script src="js/admin-images-view.js"></script>';
require_once __DIR__ . '/bottom.php';

