<?php
require_once __DIR__ . '/../connect.php';
ini_set('display_errors', 0);

$sName = $_POST['txtSignupName'] ?? '';
if (empty($sName)) {
    sendResponse(0, __LINE__, 'Name missing');
}
if (strlen($sName) < 2) {
    sendResponse(0, __LINE__, 'Name too short');
}
if (strlen($sName) > 50) {
    sendResponse(0, __LINE__, 'Name too long');
}


$sNickName = $_POST['txtSignupNickname'] ?? '';
if (empty($sNickName)) {
    sendResponse(0, __LINE__, 'Nickname missing');
}


try {
    $stmt = $db->prepare('SELECT nickname FROM users WHERE nickname = :sNickName');
    $stmt->bindValue(':sNickName', $sNickName);
    $stmt->execute();
    $iRowsAffected = $stmt->rowCount();
    if ($iRowsAffected === 1) {
        sendResponse(0, __LINE__, 'This nickname is already taken');
    }
} catch (PDOException $e) {
    echo $e;

    exit();
}


if (strlen($sNickName) < 2) {
    sendResponse(0, __LINE__, 'Nickname too short');
}
if (strlen($sNickName) > 30) {
    sendResponse(0, __LINE__, 'Nickname too long');
}


$sEmail = $_POST['txtSignupEmail'] ?? '';
if (empty($sEmail)) {
    sendResponse(0, __LINE__, 'E-mail missing');
}

try {
    $stmt = $db->prepare('SELECT email FROM users WHERE email = :sEmail');
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->execute();
    $iRowsAffected = $stmt->rowCount();
    if ($iRowsAffected === 1) {
        sendResponse(0, __LINE__, 'This e-mail is already taken');
    }

} catch (PDOException $e) {
    echo $e;

    exit();
}


if (strlen($sEmail) < 5) {
    sendResponse(0, __LINE__, 'E-mail too short');
}
if (strlen($sEmail) > 100) {
    sendResponse(0, __LINE__, 'E-mail too long');
}
if (!filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
    sendResponse(0, __LINE__, 'Not a valid Email');
}

$sPassword = $_POST['txtSignupPassword'] ?? '';
if (empty($sPassword)) {
    sendResponse(0, __LINE__, 'Password missing');
}
if (strlen($sPassword) < 4) {
    sendResponse(0, __LINE__, 'Password too short');
}
if (strlen($sPassword) > 50) {
    sendResponse(0, __LINE__, 'Password too long');
}

$sConfirmPassword = $_POST['txtSignupConfirmPassword'] ?? '';
if (empty($sConfirmPassword)) {
    sendResponse(0, __LINE__, 'Confirm password missing');
}
if ($sPassword != $sConfirmPassword) {
    sendResponse(0, __LINE__, "Passwords don't match");
}

$sActivationKey = uniqid();

try {
    $stmt = $db->prepare('INSERT INTO users VALUES 
    (NULL, :sName, :sNickName, :sEmail, :sPassword, :sActivationKey, 0, 0)');
    $stmt->bindValue(':sName', $sName);
    $stmt->bindValue(':sNickName', $sNickName);
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->bindValue(':sPassword', password_hash($sPassword, PASSWORD_DEFAULT));
    $stmt->bindValue(':sActivationKey', $sActivationKey);
    $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}


$sActivateEmailUrl = 'http://veru.dk/goodboy/apis/api-authentication-email?activationEmail=' . $sEmail . '&activationKey=' . $sActivationKey;

$sSubject = 'activation';
if (!mail($sEmail, $sSubject, $sActivateEmailUrl)) {
    sendResponse(0, __LINE__, "Could't send e-mail");
}


sendResponse(1, __LINE__, "You are signed up");


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit;
}
























