<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

require_once __DIR__ . '/../connect.php';

try {
    $stmt = $db->prepare('SELECT images.id as image_id, points.points_amount
    FROM images
    JOIN points ON images.id = points.image_fk 
    WHERE points.user_fk = :sUserId
    GROUP BY images.id, points.points_amount');
    $stmt->bindValue(':sUserId', $sUserId);
    $stmt->execute();
    $aRows = $stmt->fetchAll();
} catch (PDOException $e) {
    echo $e;
    exit();
}

echo json_encode($aRows);
