<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}

$sUserId = $_SESSION['sUserId'];

$sName = $_POST['sUserName'] ?? '';
if (empty($sName)) {
    sendResponse(0, __LINE__, 'Name missing');
}
if (strlen($sName) < 2) {
    sendResponse(0, __LINE__, 'Name too short');
}
if (strlen($sName) > 50) {
    sendResponse(0, __LINE__, 'Name too long');
}


$sNickName = $_POST['sUserNickName'] ?? '';
if (empty($sNickName)) {
    sendResponse(0, __LINE__, 'Nickname missing');
}
if (strlen($sNickName) < 2) {
    sendResponse(0, __LINE__, 'Nickname too short');
}
if (strlen($sNickName) > 30) {
    sendResponse(0, __LINE__, 'Nickname too long');
}

$sEmail = $_POST['sUserEmail'] ?? '';
if (empty($sEmail)) {
    sendResponse(0, __LINE__, 'E-mail missing');
}
if (strlen($sEmail) < 5) {
    sendResponse(0, __LINE__, 'E-mail too short');
}
if (strlen($sEmail) > 100) {
    sendResponse(0, __LINE__, 'E-mail too long');
}
if (!filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {
    sendResponse(0, __LINE__, 'Not a valid Email');
}


require_once __DIR__ . '/../connect.php';
try {
    $stmtUpdateProfileInfo = $db->prepare('UPDATE users SET name = :sName, nickname = :sNickname, email = :sEmail WHERE id = :sUserId');
    $stmtUpdateProfileInfo->bindValue(':sName', $sName);
    $stmtUpdateProfileInfo->bindValue(':sNickname', $sNickName);
    $stmtUpdateProfileInfo->bindValue(':sEmail', $sEmail);
    $stmtUpdateProfileInfo->bindValue(':sUserId', $sUserId);
    $stmtUpdateProfileInfo->execute();
    $iUpdateRowCount = $stmtUpdateProfileInfo->rowCount();
} catch (PDOException $e) {
    echo $e;
}
sendResponse(1, __LINE__, 'Your information have been updated.');


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}
