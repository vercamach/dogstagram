<?php
ini_set('display_errors', 0);

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sImageId = $_GET['sImageId'];
if (empty($sImageId)) {
    sendResponse(0, __LINE__, "sImageId is missing.");
}
$sPoints = $_GET['sPoints'];
if (empty($sPoints)) {
    sendResponse(0, __LINE__, "Couldn't get the points.");
}

require_once __DIR__ . '/../connect.php';

try {
    $stmt = $db->prepare('SELECT * FROM points WHERE user_fk = :sUser_fk AND image_fk = :sImage_fk');
    $stmt->bindValue(':sUser_fk', $sUserId);
    $stmt->bindValue(':sImage_fk', $sImageId);
    $stmt->execute();
    $aRow = $stmt->fetch();
    $iAmountOfRows = $stmt->rowCount();
} catch (PDOException $e) {
    echo $e;
}

if ($iAmountOfRows == 0) {
    try {
        $stmt = $db->prepare('INSERT INTO points (user_fk, image_fk, points_amount) VALUES (:sUser_fk ,:sImage_fk, :sPoints)');
        $stmt->bindValue(':sUser_fk', $sUserId);
        $stmt->bindValue(':sImage_fk', $sImageId);
        $stmt->bindParam(':sPoints', $sPoints);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
    }
    sendResponse(1, __LINE__, 'Points saved');
};

if ($aRow->points_amount != $sPoints) {
    try {
        $stmt = $db->prepare('UPDATE points SET points_amount = :sPoints WHERE user_fk = :sUser_fk AND image_fk = :sImage_fk');
        $stmt->bindParam(':sPoints', $sPoints);
        $stmt->bindValue(':sUser_fk', $sUserId);
        $stmt->bindValue(':sImage_fk', $sImageId);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
    }
    sendResponse(1, __LINE__, 'Points updated');
}


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}





