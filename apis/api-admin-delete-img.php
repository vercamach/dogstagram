<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: ../login');
}
$sUserId = $_SESSION['sUserId'];
if (empty($_GET['sImageId'])) {
    sendResponse(0, __LINE__, "Couldn't get the image id");
};
$sImageId = $_GET['sImageId'];

require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('SELECT is_admin FROM users WHERE id = :sUserId');
    $stmt->bindValue(':sUserId', $sUserId);
    $stmt->execute();
    $bIsAdmin = $stmt->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($bIsAdmin->is_admin != 1) {
    sendResponse(0, __LINE__, "Only admin can delete this picture");
}
try {
    $stmt = $db->prepare('DELETE FROM images WHERE id = :sImageId');
    $stmt->bindValue(':sImageId', $sImageId);
    $stmt->execute();
    $sRowsAffected = $stmt->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($sRowsAffected !== 1) {
    sendResponse(0, __LINE__, "Couldn't delete the image");

} else {
    sendResponse(1, __LINE__, "The image was deleted");
}


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}
