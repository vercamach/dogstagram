$("#frmAddDescription").submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-save-dog-information",
        data: $("#frmAddDescription").serialize(),
        dataType: "JSON"
    })
        .done(function (jData) {
            if (jData.status == 1) {
                swal({
                    title: "Saved!",
                    icon: "success"
                }).then(function () {
                    window.location = "dogstagram";
                });
            } else {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                });
            }
        })
        .fail(function () {
            console.log("error");
        });
    return false;
});