<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

if (empty($_GET['sBlockedUserId'])) {
    sendResponse(0, __LINE__, "Couldn't get the blocked users id");
};
$sBlockedUserId = $_GET['sBlockedUserId'];

require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('SELECT is_admin FROM users WHERE id = :sUserId');
    $stmt->bindValue(':sUserId', $sUserId);
    $stmt->execute();
    $bIsAdmin = $stmt->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if ($bIsAdmin->is_admin != 1) {
    sendResponse(0, __LINE__, "Only admin can block this user.");
}
try {
    $stmt = $db->prepare('UPDATE users SET active = !active WHERE id = :sBlockedUserId');
    $stmt->bindValue(':sBlockedUserId', $sBlockedUserId);
    $stmt->execute();
    $aRowBlockedUser = $stmt->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if ($aRowBlockedUser != 1) {
    sendResponse(0, __LINE__, "Couldn't block the user");
}
try {
    $stmt = $db->prepare('SELECT * FROM users WHERE id = :sBlockedUserId');
    $stmt->bindValue(':sBlockedUserId', $sBlockedUserId);
    $stmt->execute();
    $aRowUsersStatus = $stmt->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}


if ($aRowUsersStatus->active == 1) {
    sendResponse(1, __LiNE__, 'User was unblocked');
}
if ($aRowUsersStatus->active == 0) {
    sendResponse(1, __LiNE__, 'User was blocked');
}


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit();
}
