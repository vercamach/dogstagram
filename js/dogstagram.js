$(document).ready(function () {
    getPoints();
    getUsersPoints();
});

$('svg').on('click', function () {
    window.location.reload();
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            console.log(e.target);
            $('#thumbnail').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#fileToUpload").change(function () {
    readURL(this);
});


function setPoints(e, sPoints) {
    let oParent = e.parentElement;
    let aSvgs = oParent.querySelectorAll('svg');

    for (var i = 0; i < aSvgs.length; i++) {
        if (aSvgs[i] != e) {
            aSvgs[i].classList.remove('green')
        }
    }
    e.classList.toggle('green');
    let sImageId = oParent.getAttribute('data-image-id');


    $.ajax({
        method: "GET",
        url: "apis/api-set-points.php",
        data: {
            "sImageId": sImageId,
            "sPoints": sPoints
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        console.log(jData)
    }).fail(function () {
        console.log('error')
    })

}


function getPoints() {
    $.ajax({
        method: "GET",
        url: "apis/api-get-points.php",
        data: {},
        dataType: "JSON",
        cache: false
    }).done(function (ajData) {
        console.log(ajData);
        for (var i = 0; i < ajData.length; i++) {
            var sImageId = ajData[i].image_id;

            var sPoints = ajData[i].points_amount; // 0 1 2 3
            var sTotal = ajData[i].total
            $('[data-image-id=' + sImageId + ']').find('.' + sPoints).text(sTotal);
            var svg = $('svg');

        }
    }).fail(function () {

    })
}

function getUsersPoints() {
    $.ajax({
        method: "GET",
        url: "apis/api-get-current-rating.php",
        data: {},
        dataType: "JSON",
        cache: false
    }).done(function (ajData) {
        console.log(ajData);

        for (var i = 0; i < ajData.length; i++) {
            var sImageId = ajData[i].image_id;
            var sPoints = ajData[i].points_amount;
            var eee = $('[data-image-id=' + sImageId + ']').find('.' + sPoints).next().addClass('green');
            console.log(eee);
        }
    }).fail(function () {

    })
}