<?php
ini_set('display_errors', 0);

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sImageId = $_POST['sImageId'] ?? '';
if (empty($sImageId)) {
    sendResponse(0, __LINE__, 'Id is missing');
}

$sDogName = $_POST['sDogName'] ?? '';
if (empty($sDogName)) {
    sendResponse(0, __LINE__, 'Name is missing');
}

$sDogDescription = $_POST['sDogDescription'] ?? '';
if (empty($sDogDescription)) {
    sendResponse(0, __LINE__, 'Description is missing');
}
if (strlen($sDogDescription) > 500) {
    sendResponse(0, __LINE__, 'Text is too long');
}

require_once __DIR__ . '/../connect.php';
try {
    $stmt = $db->prepare('SELECT * FROM dogs_descriptions WHERE image_fk = :sImageId');
    $stmt->bindValue(':sImageId', $sImageId);
    $stmt->execute();
    $aRow = $stmt->rowCount();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if ($aRow == 0) {
    try {
        $stmtInsertInfo = $db->prepare('INSERT INTO dogs_descriptions VALUES (:sImageId, :sName, LOWER(:sDescription))');
        $stmtInsertInfo->bindValue(':sImageId', $sImageId);
        $stmtInsertInfo->bindValue(':sName', $sDogName);
        $stmtInsertInfo->bindValue(':sDescription', $sDogDescription);
        $stmtInsertInfo->execute();
        $iRowInserted = $stmtInsertInfo->rowCount();
        if ($iRowInserted == 1) {
            sendResponse(1, __LINE__, 'Information is saved');
        }
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
}

if ($aRow === 1) {
    try {
        $stmtUpdateInfo = $db->prepare('UPDATE dogs_descriptions SET name = :sName, description = LOWER(:sDescription) where image_fk = :sImageId');
        $stmtUpdateInfo->bindValue(':sName', $sDogName);
        $stmtUpdateInfo->bindValue(':sDescription', $sDogDescription);
        $stmtUpdateInfo->bindValue(':sImageId', $sImageId);
        $stmtUpdateInfo->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    if ($stmtUpdateInfo->rowCount() === 0) {
        sendResponse(0, __LINE__, 'There are some trouble updating the information');
    }
    sendResponse(1, __LINE__, 'yaa');
}


function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit;
}
