<?php
session_start();
if (!session_start()) die('Cannot start session')
ini_set('display_errors', 0);
require_once __DIR__ . '/../connect.php';


$sNickName = $_POST['txtLoginNickname'] ?? '';
if (empty($sNickName)) {
    sendResponse(0, __LINE__, 'Nickname missing');
}
if (strlen($sNickName) < 2) {
    sendResponse(0, __LINE__, 'Nickname too short');
}
if (strlen($sNickName) > 30) {
    sendResponse(0, __LINE__, 'Nickname too long');
}


$sPassword = $_POST['txtLoginPassword'];
if (empty($sPassword)) {
    sendResponse(0, __LINE__, 'Password missing');
}

try {
    $stmt = $db->prepare('SELECT id from users WHERE nickname = :sNickName');
    $stmt->bindValue(':sNickName', $sNickName);
    $stmt->execute();
    $aRowId = $stmt->fetch();
} catch (PDOException $e) {
    echo $e;
    exit();
}

if ($aRowId == false) {
    sendResponse(0, __LINE__, "Your nickname is wrong. Please try again.");
}
$sUserId = $aRowId->id;

try {
    $stmt = $db->prepare('SELECT password FROM users WHERE nickname = :sNickName');
    $stmt->bindValue(':sNickName', $sNickName);
    $stmt->execute();
    $aRowPassword = $stmt->fetch();
    $sPasswordToValidate = $aRowPassword->password;
} catch (PDOException $e) {
    echo $e;
    exit();
}

if (!password_verify($sPassword, $sPasswordToValidate)) {
    sendResponse(0, __LINE__, "Password doesn't match");
}
try {
    $stmt = $db->prepare('SELECT active FROM users WHERE nickname = :sNickName');
    $stmt->bindValue(':sNickName', $sNickName);
    $stmt->execute();
    $aActive = $stmt->fetch();
    $bIsActive = $aActive->active;
} catch (PDOException $e) {
    echo $e;
    exit();
}
if ($bIsActive != 1) {
    sendResponse(0, __LINE__, 'User is not activated.');
};


$_SESSION['sUserId'] = $sUserId;

sendResponse(1, __LINE__, 'You are logged in');

exit(); 
function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": ' . $iStatus . ', "code":' . $iLineNumber . ', "message": "' . $sMessage . '" }';
    exit;
}
































